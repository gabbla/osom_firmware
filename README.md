# Speedometer firmware

## Note

It's not possible to debug the project with `XC32 v2.10`. `XC32 v1.43` still works. It seems that there is a missing library.

## Packets

Every packet, both received or transmitted, is made as follow:

Pos | Len            | Meaning        | Value
--  | --             | --             | --
0   | 2              | Preamble       | 0x534D
2   | 1              | Source         |
3   | 1              | Destination    |
4   | 4              | Transaction ID |
8   | 4              | Message ID     |
12  | 1              | Payload length |
13  | 1              | Command ID     |
14  | Payload Length | Payload        |

A packet is characterized by a `Transaction ID` and a `Message ID`. The Transaction ID won't change during the whole message exchange. The Message ID is message specific.
In this way it's possible to track down the replies without the need to manage multiple command id. A transaction ID is kept also through devices, so if a message is addressed to both `MASTER` and `SLAVE0`, that field won't change.

### Messages

The messages types are enumerated in `firmware/src/somparser.h` in the enumeration `BLECommand`.

> Not the best name

There are 3 type groups:
- `0x00` to `0x0F` are commands reserved for the `master` only, managed by `BLEApp`;
- `0x10` to `0x4F` are generic commands to control the system;
- `0x50` to `0x??` are response messages sent by the system to the received commands;
- `0xF0` to `0xFF` are special meaning messages sent by the system

There are also some common definitions along all the exchanged messages:

#### Channels

Value | Channel
--    | --
0     | None
1     | Right
2     | Left
3     | Both

#### Run Modes

Vale | Mode
--   | --
-1   | Invalid
0    | None
1    | Positioning
2    | Free start


#### Devices

Value | Device
--    | --
0x01  | Application
0x02  | Master
0x04  | Slave 0
0x08  | Slave 1
0x10  | Slave 2
0x20  | Slave 3
0x40  | Slave 4

---

#### Group 1: from `0x00` to `0x0F`

##### `0x00` Ping

- Payload length 0

Simply check if the system is up and running. TODO response

TODO other commands

---

#### `0x10` to `0x4F`

---

##### `0x10` Mode setting

This command set the desired running mode. Some modes are single channel, some other use both.

The payload contains the following information:

Offset | Size | Meaning
--     | --   | --
0      | 1    | Mode
1      | 1    | Channel

---

##### `0x11` Start positioning

This command place the system in the so-called _positioning mode_.

- Payload length: 1

Payload data

Field|Bit|Meaning
--|--|--
Reserved|7-4|-
Channel to be used|3-0|Channel as described above

---

##### `0x12` Stop positioning

This command exits the so-called _positioning mode_.

- Payload length: 0

---

#### `0x50` to `0x??`

---

##### `0x51` Channel positioning status

This message notify the user about the channel status change.

- Payload length: 1

**Payload data**


Field|Bit|Meaning
--|--|--
Channel|7-4|Channel as described above
Status|0|Channel status

**Channel status**

Value|Meaning
--|--
0|The channel receiver is covered, check positioning
1|The channel receiver is receiving the laser

---


