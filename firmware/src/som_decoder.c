#include <stdio.h>
#include <stdbool.h>
#include "logger.h"
#include "somparser.h"

static const char *devices[] = {
    "DEV_INVALID",
    "DEV_APP",
    "DEV_MASTER",
    "DEV_SLAVE0",
    "DEV_SLAVE1",
    "DEV_SLAVE2",
    "DEV_SLAVE3",
    "DEV_SLAVE4",
};

char *dev2str(const Device dev) {
    if(dev >= DEV_APPLICATION && dev <= DEV_SLAVE4)
        return devices[dev];
    return "Other combination";
}

int main(int argc, char *argv[])
{
    if(argc < 1 + PACKET_BASE_LEN) {
        ERROR("Too few arguments");
        exit(-1);
    }
    size_t packet_len = argc - 1;
    uint8_t data[packet_len];

    for(size_t i = 0; i < packet_len; ++i)
        data[i] = (uint8_t)strtol(argv[i + 1], NULL, 0);

    Packet *p = PACKET_Get(data);

    Device src = PACKET_GetSource(p);
    Device dst = PACKET_GetDestination(p);

    printf("Source: %s [0x%02X]\n", dev2str(src), src);
    printf("Destination: %s [0x%02X]\n", dev2str(dst), dst);
    printf("Transaction ID: 0x%08X\n", PACKET_GetTransactionID(p));
    printf("Message ID: 0x%08X\n", PACKET_GetMessageID(p));
    printf("Command: 0x%02X\n", PACKET_GetCommand(p));


    return 0;
}
