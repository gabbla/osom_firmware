#include "bq27441_parser.h"

millivolts_t BQ27441_GetMillivolts(const uint8_t *raw) {
    return ((uint16_t)raw[1] << 8 | raw[0]);
}

soc_t BQ27441_GetStateOfCharge(const uint8_t *raw) {
    return ((uint16_t)raw[1] << 8 | raw[0]);
}

milliamps_t BQ27441_GetAverageCurrent(const uint8_t *raw) {
    return ((uint16_t)raw[1] << 8 | raw[0]);
}
